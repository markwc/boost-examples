PATH := /usr/local/bin:$(PATH)

default::
	@echo
	@echo "Interesting targets to make:"
	@echo "  clean     Remove build directory"
	@echo "  test      Build all code and run unit tests"
	@echo "  run       Build all code and run main"
	@echo

all:: test

clean_built: 
	@rm -rf build

clean:: clean_built

test::
	@mkdir -p build
	cd build; cmake .. && make && test/My_class_test
run::
	@mkdir -p build
	cd build; cmake .. && make && src/My_class_run



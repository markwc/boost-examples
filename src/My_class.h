/// @file My_class.h

#ifndef SRC_MY_CLASS_H
#define SRC_MY_CLASS_H

#include <memory>

namespace Katas {

class My_class {
 public:
  My_class();

  ~My_class();

  My_class(My_class &&) = delete;

  My_class(const My_class &) = delete;

  My_class &operator=(My_class &&) = delete;

  My_class &operator=(const My_class &) = delete;

 private:
  struct Impl;

  std::unique_ptr<Impl> m_p_impl;
};
}  // namespace Katas

#endif  // #define SRC_MY_CLASS_H

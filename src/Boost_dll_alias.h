/// @name Boost_dll_alias.h
#ifndef __BOOST_DLL_ALIAS_H__
#define __BOOST_DLL_ALIAS_H__

#include <memory>

namespace katas {

class Boost_dll_alias {
public:
  Boost_dll_alias();
  static auto create() -> std::shared_ptr<Boost_dll_alias>;
  virtual auto do_something() -> void;

private:
  int x;
};

} // namespace katas

#endif
/// @file main.cpp

#include <My_class.h>

#include <boost/tokenizer.hpp>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

int main()
{
  std::cout << "Main run" << std::endl;

  std::string data("test_files/data.csv");

  std::ifstream in(data.c_str());
  if (!in.is_open())
  {
    return 1;
  }

  std::string line;
  std::vector<std::string> vec;
  while (getline(in, line))
  {
    boost::tokenizer<boost::escaped_list_separator<char>> tok(line);
    vec.assign(tok.begin(), tok.end());
    if (vec.size() < 3)
    {
      continue;
    }
    copy(vec.begin(), vec.end(),
         std::ostream_iterator<std::string>(std::cout, "|"));
    std::cout << std::endl << "------------------------" << std::endl;
  }

  return 0;
}
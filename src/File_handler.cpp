/// @file File_handler.cpp

#include <File_handler.h>

#include <boost/algorithm/string.hpp>

#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace Katas
{

enum class string_value : int
{
  undefined,
  iq,
  unit,
  dwell,
  frame,
  frequency_MHz
};

std::map<std::string, string_value> token_map{
    {"iq", string_value::iq},
    {"unit", string_value::unit},
    {"dwell", string_value::dwell},
    {"frameNum", string_value::frame},
    {"MHz", string_value::frequency_MHz}};

static const int int_base{10};

struct File_handler::Impl
{
  Impl() = default;

  ~Impl() = default;

  Impl(Impl &&) = delete;

  Impl(const Impl &) = delete;

  Impl &operator=(Impl &&) = delete;

  Impl &operator=(const Impl &) = delete;

  void trim_extension(std::string &file_name)
  {
    auto pos = file_name.rfind(".");
    file_name.erase(pos);
  }

  std::vector<std::string> split(const std::string &s, const std::string &delim)
  {
    std::vector<std::string> result;
    size_t start = 0U;
    size_t end = 0U;
    while (end != std::string::npos)
    {
      end = s.find(delim, start);
      result.push_back(s.substr(start, end - start));
      start = end + delim.length();
    }
    return result;
  }
};

//-----------------------------------------------------------------------------

File_handler::File_handler() : m_p_impl(std::make_unique<Impl>())
{
}

File_handler::~File_handler() = default;

void File_handler::process_file_name(std::string file_name, uint32_t &tsi,
                                     uint64_t &tsf, uint32_t &unit,
                                     uint32_t &dwell, uint32_t &frame,
                                     uint32_t &frequency_MHz)
{
  std::vector<std::string> tokens;
  std::string delims{"_,."};
  boost::split(tokens, file_name, boost::is_any_of(delims));

  string_value current_token_value{string_value::undefined};
  bool grab_next = false;
  for (auto token = tokens.begin(); token != tokens.end(); token++)
  {
    int token_value = static_cast<int>(token_map[*token]);
    switch (token_value)
    {
    case static_cast<int>(string_value::iq):
      tsi = std::strtoul((++token)->c_str(), nullptr, int_base);
      tsf = std::strtoull((++token)->c_str(), nullptr, int_base);
      break;
    case static_cast<int>(string_value::unit):
      unit = std::strtoul((++token)->c_str(), nullptr, int_base);
      break;
    case static_cast<int>(string_value::dwell):
      dwell = std::strtoul((++token)->c_str(), nullptr, int_base);
      break;
    case static_cast<int>(string_value::frame):
      frame = std::strtoul((++token)->c_str(), nullptr, int_base);
      break;
    case static_cast<int>(string_value::frequency_MHz):
      frequency_MHz = std::strtoul((++token)->c_str(), nullptr, int_base);
      break;
    default:
      break;
    }
  }
}
} // namespace Katas

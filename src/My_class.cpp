/// @file My_class.cpp

#include <My_class.h>

namespace Katas {

struct My_class::Impl {
  Impl() = default;

  ~Impl() = default;

  Impl(Impl &&) = delete;

  Impl(const Impl &) = delete;

  Impl &operator=(Impl &&) = delete;

  Impl &operator=(const Impl &) = delete;
};

//-----------------------------------------------------------------------------

My_class::My_class() : m_p_impl(std::make_unique<Impl>()) {}

My_class::~My_class() = default;

}  // namespace Katas

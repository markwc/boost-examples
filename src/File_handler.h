/// @file File_handler.h

#ifndef SRC_FILE_HANDLER_H
#define SRC_FILE_HANDLER_H

#include <memory>

namespace Katas
{

class File_handler
{
public:
  File_handler();

  ~File_handler();

  File_handler(File_handler &&) = delete;

  File_handler(const File_handler &) = delete;

  File_handler &operator=(File_handler &&) = delete;

  File_handler &operator=(const File_handler &) = delete;

  void process_file_name(std::string file_name, uint32_t &tsi, uint64_t &tsf,
                         uint32_t &unit, uint32_t &dwell, uint32_t &frame,
                         uint32_t &frequency_MHz);

private:
  struct Impl;

  std::unique_ptr<Impl> m_p_impl;
};
} // namespace Katas

#endif // #define SRC_FILE_HANDLER_H

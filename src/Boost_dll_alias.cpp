/// @name Boost_dll_alias.cpp
#include "Boost_dll_alias.h"
#include <boost/log/trivial.hpp>
#include <boost/dll/alias.hpp>
#include <iostream>

using namespace katas;

const int init_val = 20;

Boost_dll_alias::Boost_dll_alias() : x(init_val) {
}

auto Boost_dll_alias::create() -> std::shared_ptr<Boost_dll_alias> {
  return std::make_shared<Boost_dll_alias>();
}

auto Boost_dll_alias::do_something() -> void {
  BOOST_LOG_TRIVIAL(info) << "x = " << x;
}

BOOST_DLL_ALIAS(Boost_dll_aliasMy_class::create, Boost_dll_alias_create)

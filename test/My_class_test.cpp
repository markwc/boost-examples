#include <My_class.h>
#include <gtest/gtest.h>

#include <array>
#include <random>

namespace {

class My_class_test : public ::testing::Test {
 public:
  My_class_test() = default;

  ~My_class_test() override = default;

  My_class_test(My_class_test &&) = delete;

  My_class_test(const My_class_test &) = delete;

  My_class_test &operator=(My_class_test &&) = delete;

  My_class_test &operator=(const My_class_test &) = delete;

 protected:
  void SetUp() override {}

  void TearDown() override {}
};

TEST_F(My_class_test, instantiate) {
  Katas::My_class UUT;
  ASSERT_TRUE(true);
}

TEST_F(My_class_test, create) {
  auto p_UUT = std::make_unique<Katas::My_class>();
  ASSERT_NE(nullptr, p_UUT);
}
}  // namespace
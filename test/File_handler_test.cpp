#include <gtest/gtest.h>

#include <File_handler.h>

#include <string>

namespace
{

class File_handler_test : public ::testing::Test
{
public:
  File_handler_test() = default;

  ~File_handler_test() override = default;

  File_handler_test(File_handler_test &&) = delete;

  File_handler_test(const File_handler_test &) = delete;

  File_handler_test &operator=(File_handler_test &&) = delete;

  File_handler_test &operator=(const File_handler_test &) = delete;

protected:
  void SetUp() override
  {
  }

  void TearDown() override
  {
  }

};

TEST_F(File_handler_test, do_it)
{
  std::string file_name("file_frame_iq_001_002_003_unit_004_dwell_005_"
                        "frameNum_006_MHz_007.bin ");
  std::string original_file_name(file_name);
  uint32_t tsi = 0U;
  uint64_t tsf = 0UL;
  uint32_t unit = 0U;
  uint32_t dwell = 0U;
  uint32_t frame = 0U;
  uint32_t frequency_MHz = 0U;
  Katas::File_handler handler;
  handler.process_file_name(file_name, tsi, tsf, unit, dwell, frame, frequency_MHz);
  EXPECT_EQ(1U, tsi);
  EXPECT_EQ(2UL, tsf);
  EXPECT_EQ(4U, unit);
  EXPECT_EQ(5U, dwell);
  EXPECT_EQ(6U, frame);
  EXPECT_EQ(7U, frequency_MHz);
  EXPECT_EQ(file_name, original_file_name);
}

} // namespace
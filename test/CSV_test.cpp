#include <gtest/gtest.h>

#include <boost/tokenizer.hpp>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace
{

std::string CSV_file("data.csv");

class CSV_test_test : public ::testing::Test
{
public:
  CSV_test_test() = default;

  ~CSV_test_test() override = default;

  CSV_test_test(CSV_test_test &&) = delete;

  CSV_test_test(const CSV_test_test &) = delete;

  CSV_test_test &operator=(CSV_test_test &&) = delete;

  CSV_test_test &operator=(const CSV_test_test &) = delete;

  void make_CSV_file()
  {
    std::ofstream file;
    file.open(CSV_file);
    ASSERT_TRUE(file.is_open());
    file << "number,string" << std::endl;
    file << "1.0,test string" << std::endl;
    file.close();
  }

protected:
  void SetUp() override
  {
  }

  void TearDown() override
  {
  }
};

TEST_F(CSV_test_test, read_from_file)
{
  make_CSV_file();

  std::ifstream in(CSV_file.c_str());
  ASSERT_TRUE(in.is_open());

  std::string line;
  std::vector<std::string> vec;
  while (getline(in, line))
  {
    boost::tokenizer<boost::escaped_list_separator<char>> tok(line);
    vec.assign(tok.begin(), tok.end());
    ASSERT_EQ(2U, vec.size());
    copy(vec.begin(), vec.end(),
         std::ostream_iterator<std::string>(std::cout, "|"));
    std::cout << std::endl << "------------------------" << std::endl;
  }
}

} // namespace
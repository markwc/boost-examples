/// @name main.cpp
#include "my_class.h"
#include <boost/dll/import.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>

using namespace std;
using namespace katas;

auto init_logging() {
  namespace logging = boost::log;
  logging::core::get()->set_filter(logging::trivial::severity >=
                                   logging::trivial::info);
}

int main(int argc, char** argv) {
  init_logging();
  BOOST_LOG_TRIVIAL(info) << "================================";
  BOOST_LOG_TRIVIAL(info) << "Testing boost::dll::import_alias";
  BOOST_LOG_TRIVIAL(info) << "================================";
  auto library = boost::filesystem::path("libmy_class.so");
  auto create = std::string("my_class_create");
  auto entry =
      boost::dll::import_alias<decltype(My_class::create)>(library, create);
  auto my_object = entry();
  my_object->do_something();
}
